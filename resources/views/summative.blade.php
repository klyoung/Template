@php
    $rowCount = 1;

    $user = $data['style'][0]['user'];

    $results = $data['style'][0]['results'];

@endphp

<style>

    body
    {
        font-family: Arial, sans-serif;
        margin-top: 10px;
    }

    table, td, th
    {
        border: 1px solid grey;
        border-collapse: collapse;
    }

    td, th
    {
        padding: 2px;
    }

    .header
    {
        font-weight: bold;
        margin: 0;
        padding: 0;
    }

    .headerContainer
    {
        width: 100%;
        margin: auto;
    }

    .logoImage
    {
        width: 50%;
        float: left;
    }

    .logoImage img
    {
        width: 500px;
    }

    .details
    {
        float: left;
        margin-top: 40px;
        margin-bottom: 10px;
        margin-left: 50px;
    }

    .details p
    {
        text-align: left;
    }

    .blockText
    {
        margin: 0;
        padding: 0;
    }


</style>

<div id="mainDiv" style="margin:auto; text-align: center;">


    <div class="headerContainer">

        <div class="logoImage">

            <img src="https://workflow.castlebranch.com/images/bridges-logo.png" alt="CastleBranch Bridges">

        </div>

        <div class="details">

            <p class="header"> {{ ucfirst($data['style'][0]['template']) .' Report'}}  </p>

            <p class="blockText"> {{ $user['name'] }} </p>

            @foreach($user['etc'] as $u)

                @foreach($u as $key => $value)

                    <p class="blockText">{{ $key .': '. $value }}</p>

                @endforeach

            @endforeach

        </div>

    </div>

    <table style="width:100%">

        <tr>

            <th>#</th>

            <th>Question</th>

            <th>Answer</th>

        </tr>

        @foreach($results as $result)

            @foreach($result as $question => $answer)

                @if ($rowCount % 45 == 0)

    </table>

    <div class="headerContainer">

        <div class="logoImage">

            <img src="https://workflow.castlebranch.com/images/bridges-logo.png" alt="CastleBranch Bridges">

        </div>

        <div class="details">

            <p class="header"> {{ ucfirst($data['style'][0]['template']) .' Report'}}  </p>

            <p class="blockText"> {{ $user['name'] }} </p>

            @foreach($user['etc'] as $u)

                @foreach($u as $key => $value)

                    <p class="blockText">{{ $key .': '. $value }}</p>

                @endforeach

            @endforeach

        </div>

    </div>

    <table style="width:100%">

        <tr>

            <th>#</th>

            <th>Question</th>

            <th>Answer</th>

        </tr>

        @endif
        <tr>

            <td> {{$rowCount++}} </td>

            <td> {{ $question }} </td>

            <td> {{ $answer }} </td>

        </tr>

        @endforeach

        @endforeach

    </table>
</div>

