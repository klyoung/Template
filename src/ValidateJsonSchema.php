<?php

namespace Klyoung\Template;

use League\JsonGuard\Validator;
use League\JsonReference\Dereferencer;
use League\JsonReference\SchemaLoadingException;

require(__DIR__ . '/../vendor/autoload.php');

class ValidateJsonSchema extends ErrorResponse
{
    /**
     * Validate json data against json
     * schema based off of summative type.
     *
     * @param $data
     * @param $template
     * @return bool|\League\JsonGuard\ValidationError[]
     */
    public function validate($data, $template)
    {
        $dereferencer  = new Dereferencer();

        try {
            $schema = $dereferencer->dereference('file://'. __DIR__ .'/json/' . $template . '_schema.json');
        } catch (SchemaLoadingException $e) {
            return $template . " is not a valid template.";
        }

        $validator = new Validator(json_decode(json_encode($data)), $schema);

        if ($validator->passes()) {
            return true;
        }

        return $this->errorMessage($validator->errors());
    }
}