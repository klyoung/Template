<?php
namespace Klyoung\Template;

class Templates
{
    public function getTemplate($data, $template)
    {
        return ["base64" => base64_encode(view('template::' . $template , ['data' => $data])->render()), "type" => "html"];
    }
}
