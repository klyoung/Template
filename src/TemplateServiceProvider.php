<?php

namespace Klyoung\Template;

use Illuminate\Support\ServiceProvider;

class TemplateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/./../resources/views', 'template');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('castlebranch-templates', function() {
            return new Templates();
        });

        $this->app->bind('castlebranch-json-validator', function() {
            return new ValidateJsonSchema();
        });
    }
}