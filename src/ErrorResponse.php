<?php

namespace Klyoung\Template;

class ErrorResponse {

    public function errorMessage ($error) {
        $error = json_decode(json_encode($error[0]), true);

        return $error['data_path'] . ' must be of type ' . $error['schema']['type'];
    }
}
