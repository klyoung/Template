<?php

namespace Klyoung\Template\Facades;

use Illuminate\Support\Facades\Facade;

class Templates extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'castlebranch-templates';
    }
}
