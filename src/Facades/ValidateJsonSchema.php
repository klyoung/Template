<?php

namespace Klyoung\Template\Facades;

use Illuminate\Support\Facades\Facade;

class ValidateJsonSchema extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'castlebranch-json-validator';
    }
}